# Physics

A rewrite of [Physics Simulation Kit](https://physics.rubenwardy.com) into
TypeScript, improving the simulation quality.

Try it: https://physics.rubenwardy.com/beta/gravity.html

License: GPLv3.


## Usage

```bash
npm install
npm start
````
