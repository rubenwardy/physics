const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { readdirSync, readFileSync } = require('fs');

const dest = path.resolve(__dirname, "dist");

const entry = {
	home: "./src/home/home.ts",
	style: "./src/style.ts",
};

const sims = [];

const plugins = [
	new CopyPlugin([
		{ from: "src/public", to: dest },
	]),
	new MiniCssExtractPlugin({
		// Options similar to the same options in webpackOptions.output
		// both options are optional
		filename: "[name].css",
		chunkFilename: "[id].css",
	}),
	new HtmlWebpackPlugin({
		title: "Physics",
		chunks: ["home", "style"],
		filename: "index.html",
		template: "src/templates/index.ejs",
		sims: sims
	})
];


readdirSync("src/sims", { withFileTypes: true })
	.filter(dirent => dirent.isDirectory())
	.map(dirent => dirent.name)
	.forEach(dir => {
		const path =`./src/sims/${dir}`;
		const config = JSON.parse(readFileSync(`${path}/sim.json`));

		const name = config.title.toLowerCase().replace(/ /g, "_")
		entry[name] = `${path}/App.ts`;

		config.name = name;
		config.url = `${name}.html`;
		sims.push(config);

		const chunks = [...config.depends];
		chunks.push(name);

		plugins.push(new HtmlWebpackPlugin({
			title: config.title,
			description: config.description,
			chunks: chunks,
			filename: config.url,
			template: "src/templates/sim.ejs",
		}));
	});


module.exports = {
	entry: entry,
	plugins: plugins,
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: [
					{
						loader: "ts-loader",
						options: {
							configFile: "tsconfig.json",
						}
					}
				],
				exclude: /node_modules/
			},
			{
				test: /\.handlebars$/,
				loader: "handlebars-loader"
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					MiniCssExtractPlugin.loader,
					"css-loader",
					"sass-loader"
				],
			},
		]
	},
	resolve: {
		extensions: [ ".ts", ".tsx", ".js" ],
		modules: [
			path.resolve(__dirname, "src"),
			"node_modules"
		]

	},
	output: {
		filename: "[name].js",
		path: dest
	},
	mode: "development"
};
