interface Sim {
	url: string;
	title: string;
	description?: string;
	thumbnail?: string;
}

function groupBy(key: string, array: any[]) {
	return array.reduce(function(rv: Object, x: Object) {
		(rv[x[key]] = rv[x[key]] || []).push(x);
		return rv;
	}, {});
};

function simToHTML(sim: Sim) {
	const img = sim.thumbnail ? `<img src="${sim.thumbnail}">` : "";
	const gradient = "";
	// "background: linear-gradient(to right, #654ea3, #eaafc8);";
	return `
		<li>
			<a class="sim" href="${sim.url}">
				<div class="image" style="${gradient}">
					${img}
				</div>
				<h3 class="sim_title">${sim.title}</h3>
				<p class="sim_desc">
					${sim.description || ''}
				</p>
			</a>
		</li>`;
}

const titles = {
	"Motion": "Mechanics and Motion",
	"Space": "Space and Astrophysics",
}

function setSimList(list: Sim[]) {
	const sections = groupBy("category", list);

	const content = Object.keys(sections).sort().map(name => {
		const sectionBody = sections[name]
				.filter(sim => !sim.hidden)
				.map(simToHTML).join("");

		const title = titles[name] || name;

		return `<li>
			<h2>${title}</h2>
			<ul class="sim_container">${sectionBody}</ul>
		</li>`;
	}).join("");

	const container = document.getElementById("sections");
	container.innerHTML = content;
}

declare var simulations: Sim[];

setSimList(simulations);

document.getElementById("loading").style.display = "none";
