import { appendElement } from "./utils";

export class BottomPanel {
	protected root: HTMLElement
	protected buttons = new Map<string, HTMLElement>();

	constructor(public parent: Element) {
		this.root = document.createElement("ul");
		this.root.id = "bottompanel";
		parent.appendChild(this.root);
	}

	add(html: string): HTMLElement {
		return appendElement(this.root, html);
	}

	addButton(text: string, callback: () => void = null): HTMLElement {
		const ele = this.add(`<li><a class="btn">${text}</a></li>`);
		const btn = ele.children.item(0) as HTMLElement;

		this.buttons.set(text, btn);
		if (callback) {
			this.on(text, callback);
		}
		return btn;
	}

	on(name: string, callback: () => void) {
		this.buttons.get(name).addEventListener("click", callback);
	}

	addSeparator() {
		this.add(`<li>|</li>`);
	}

	addToggleTheme() {
		const toggletheme = this.addButton("Light Mode");
		toggletheme.addEventListener("click", () => {
			this.parent.classList.toggle("light_mode");
			if (this.parent.classList.contains("light_mode")) {
				toggletheme.innerText = "Dark Mode";
			} else {
				toggletheme.innerText = "Light Mode";
			}
		});
	}

	addSpeedChange() {
		this.add(`<li>
			Speed:
			<ul class="btn-group">
				<li><a id="slower">-</a></li>
				<li><span id="timespeed" title="Simulated days per real-life seconds"><b>30</b> days/s</span></li>
				<li><a id="faster">+</a></li>
			</ul>
		</li>`)
	}

	setTimeSpeed(speed: number) {
		const timespeed = document.getElementById("timespeed");
		timespeed.innerHTML = `<b>${Math.round(speed*10)/10}</b> days/s`;
	}

	onSpeedChange(callback: (increased: boolean) => void) {
		document.getElementById("faster").addEventListener("click", () => callback(true));
		document.getElementById("slower").addEventListener("click", () => callback(false));
	}

	addStatus() {
		this.add(`<li id="status">Loading</li>`);
	}

	setStatus(status: string) {
		const ele = document.getElementById("status");
		ele.innerText = status;
	}

	addPausePlay() {
		this.addButton("Pause");
	}

	onPausePlay(callback: () => void) {
		this.on("Pause", callback);
	}

	setPaused(paused: boolean) {
		const pauseplay = this.buttons.get("Pause");
		pauseplay.innerText = paused ? "Play" : "Pause";
	}
}
