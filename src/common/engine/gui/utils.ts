import { Vector2 } from "common/calc/Vector2";

export function listenVectorInputSet(field: HTMLElement, callback: (vec: Vector2) => void) {
	const components = field.getElementsByTagName("input");

	const onChange = () => {
		const x = parseInt(components[0].value);
		const y = parseInt(components[1].value);
		if (!isNaN(x) && !isNaN(y)) {
			callback(new Vector2(x, y));
		}
	}

	for (let component of components) {
		component.addEventListener("change", onChange);
	}
}

export function setVectorInputSet(field: HTMLElement, value: Vector2) {
	const f = 10000;
	const components = field.getElementsByTagName("input");
	components[0].value = (Math.round(value.x * f) / f).toString();
	components[1].value = (Math.round(value.y * f) / f).toString();
}

export function createElements(html: string): HTMLCollection {
	const base = document.createElement("div");
	base.innerHTML = html;
	return base.children;
}

export function createElement(html: string): HTMLElement {
	return createElements(html)[0] as HTMLElement;
}

export function appendElements(parent: Element, html: string) {
	const elements = createElements(html);
	for (let element of elements) {
		parent.append(element);
	}
}

export function appendElement(parent: Element, html: string): HTMLElement {
	const element = createElement(html);
	parent.append(element);
	return element;
}
