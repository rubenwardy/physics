import { Vector2 } from "common/calc/Vector2";
import { appendElement, setVectorInputSet, listenVectorInputSet, appendElements } from "./utils";

export interface PanelField<T> {
	/**
	 * The backing HTML element
	 */
	getElement(): HTMLElement

	/**
	 * Get current value
	 */
	get(): T;

	/**
	 * Set current value
	 *
	 * @param value
	 */
	set(value: T): void;

	/**
	 * Update property in "data" when field is updated.
	 *
	 * @param data
	 */
	bind(data: any): void;

	/**
	 * Callback when value is modified
	 *
	 * @param callback
	 */
	onChange(callback: (v: T) => void): void;
}


export class PanelBuilder<T extends object> {
	constructor(private currentParent: Element,
			private fields: Map<string, PanelField<any>>) {}

	addWithLabel(label: string, unit: string, html: string): HTMLElement {
		const container = document.createElement("div");
		container.classList.add("field");
		container.innerHTML = html;

		if (unit) {
			label = `${label} <span class="unit">${unit}</span>`;
		}

		const labelE = document.createElement("label");
		labelE.innerHTML = label;
		container.prepend(labelE);

		this.currentParent.append(container);

		return container.children[1] as HTMLElement;
	}

	addButton(text: string, callback: (button: HTMLElement) => void) {
		const button = appendElement(this.currentParent,
		`<button class="btn">${text}</button>`);

		button.addEventListener("click", () => {
			callback(button)
		});
	}

	addVector(name: string, label: string, unit?: string): PanelField<Vector2> {
		const field = this.addWithLabel(label, unit, `
			<div class="row row-narrow">
				<span>(</span>
				<input type="number" size=8 step="any">
				<span>,</span>
				<input type="number" size=8 step="any">
				<span>)</span>
			</div>
		`);

		const dialogField: PanelField<Vector2> = {
			getElement: () => field,
			get: () => null,
			set: (value: Vector2) => {
				setVectorInputSet(field, value);
			},
			bind: (data: T) => {
				listenVectorInputSet(field, (v: Vector2) => {
					data[name] = v as any;
				});
			},
			onChange: (callback: (v: Vector2) => void) => {
				setTimeout(() => {
					listenVectorInputSet(field, callback);
				}, 0);
			},
		};

		this.fields.set(name, dialogField);
		return dialogField;
	}

	private addTypedInput<R>(html: string, converter: (str: string) => R,
			name: string, label: string, unit?: string): PanelField<R> {
		const field = this.addWithLabel(label, unit, html) as HTMLInputElement;

		const dialogField: PanelField<R> = {
			getElement: () => field,
			get: () => null,
			set: (value: R) => {
				if (value && typeof(value) == "number") {
					field.value = value.toFixed(2);
				} else {
					field.value = value ? value.toString() : "";
				}
			},
			bind: (data: T) => {
				field.addEventListener("change", () => {
					data[name] = converter(field.value) as any;
				});
			},
			onChange: (callback: (v: R) => void) => {
				setTimeout(() => {
					field.addEventListener("change", () => {
						callback(converter(field.value));
					});
				}, 0);
			},
		};

		this.fields.set(name, dialogField);
		return dialogField;
	}

	addNumberInput(name: string, label: string, unit?: string): PanelField<number> {
		return this.addTypedInput<number>(`<input type="number" step="any">`,
				parseFloat, name, label, unit);
	}

	addTextInput(name: string, label: string): PanelField<string> {
		return this.addTypedInput<string>(`<input type="text">`,
				(x) => x, name, label);
	}

	row(): PanelBuilder<T> {
		const row = appendElement(this.currentParent, `<div class="row"></div>`);
		return new PanelBuilder<T>(row, this.fields);
	}

	multiRow(): PanelBuilder<T> {
		const row = appendElement(this.currentParent, `<div class="row row-wrap"></div>`);
		return new PanelBuilder<T>(row, this.fields);
	}

	append(html: string)  {
		appendElements(this.currentParent, html);
	}
}


export class Panel<T extends object> {
	protected fields = new Map<string, PanelField<any>>();

	constructor(protected root: Element) {}

	remove() {
		this.root.remove();
	}

	edit(): PanelBuilder<T> {
		return new PanelBuilder<T>(this.root, this.fields);
	}

	setPlaceholder(name: string, placeholder: string) {
		const field = this.fields.get(name);
		if (!field) {
			return;
		}

		field.getElement().setAttribute("placeholder", placeholder);
	}

	/**
	 * Produces an object which looks the same as `data`, but will update this
	 * dialog and be updated by this dialog, using two-way data binding.
	 *
	 * Eg:
	 *
	 *     this.model = this.dialog.bind(this.model);
	 *
	 * @param data Input data class
	 * @returns bound binder
	 */
	bind(data: T): T {
		const binder = {};
		Object.setPrototypeOf(binder, data);

		const properties = [];
		{
			let obj = data;
			do {
				Object.getOwnPropertyNames(obj).forEach(function(prop) {
					if (properties.indexOf(prop) === -1) {
						properties.push(prop);
					}
				});
			} while (obj = Object.getPrototypeOf(obj));
		}

		properties.forEach(key => {
			const field = this.fields.get(key);
			if (!field) {
				return;
			}

			const type = typeof(data[key]);
			if (data[key] != null && type != "string" && type != "number" &&
					!(data[key] instanceof Vector2)) {
				console.error(`Unable to bind ${key}, unsupported type`);
				return;
			}

			Object.defineProperty(binder, key, {
				get: () => {
					return data[key];
				},

				set: (value: any) => {
					console.assert(value != null);
					data[key] = value;
					field.set(data[key]);
				}
			});

			field.set(data[key]);
			field.bind(data);
		});

		return binder as T
	}
}
