import { Panel, PanelBuilder } from ".";

export class Dialog<T extends object> extends Panel<T> {
	constructor(title: string) {
		const root = document.createElement("div");
		root.classList.add("panel");
		root.classList.add("dialog");
		root.innerHTML = `
			<div class="dialog-head">
				<h2>
					${title}
				</h2>

				<a class="dialog-minimise">&dash;</a>
				<a class="dialog-close">x</a>
			</div>
			<div class="dialog-body"></div>
		`;

		super(root);

		const minimiseButton = this.root.getElementsByClassName("dialog-minimise")[0];
		minimiseButton.addEventListener("click", () => {
			this.root.classList.toggle("dialog-minimised");
			const isMinimised = this.root.classList.contains("dialog-minimised");
			minimiseButton.innerHTML = isMinimised ? "+" : "&dash;";
		});

		document.getElementById("canvas").parentElement.appendChild(this.root);
	}

	edit(): PanelBuilder<T> {
		const parent = this.root.getElementsByClassName("dialog-body")[0];
		return new PanelBuilder<T>(parent, this.fields);
	}

	disableClose() {
		const button = this.root.getElementsByClassName("dialog-close")[0];
		button.remove();
	}

	minimise() {
		const minimiseButton =
				this.root.getElementsByClassName("dialog-minimise")[0];
		this.root.classList.add("dialog-minimised");
		minimiseButton.innerHTML = "+";
	}

	onClose(callback: () => void) {
		this.root.getElementsByClassName("dialog-close")[0]
				.addEventListener("click", callback);
	}
}
