import { Vector2 } from "../calc/Vector2";
import { Camera } from "./Camera";

export interface IDrawable {
	update(engine: Engine, dtime: number): void;
	draw(context: RenderingContext): void;
}

export interface Tool extends IDrawable {
	/**
	 * Activate tool: setup resources / views etc
	 */
	onActivate(): void;

	/**
	 * Deactivate tool: clean up resources / views etc
	 */
	onDeactivate(): void;

	getStatusText(): string;

	/**
	 * If true, user should deactivate tool
	 */
	isFinished: boolean;
}

export interface Engine {
	getCamera(): Camera;

	/**
	 * @returns mouse position in pixels
	 */
	getMousePosition(): Vector2;

	isKeyDown(code: number): boolean;

	isLightMode: boolean;
}

type HTML5Style = string | CanvasGradient | CanvasPattern;

export interface ColorPair {
	dark: HTML5Style;
	light: HTML5Style;
}

export type Style = HTML5Style | ColorPair;

export const EngineColors = {
	Text: { dark: "white", light: "black" },
	WidgetArrow: { dark: "yellow", light: "black" },
	InteractionCircle: { dark: "white", light: "black" },
	InteractionCircleSelected: { dark: "yellow", light: "orange" },
}

export interface RenderingContext extends Engine {
	drawText(position: Vector2, text: string, fillStyle: Style): void;
	drawCircle(position: Vector2, radius: number, fillStyle: Style, strokeStyle?: Style): void;
	drawCross(position: Vector2, size: number, strokeStyle: Style): void;
	drawLine(from: Vector2, to: Vector2, color: Style, width?: number): void;
	drawArrow(from: Vector2, to: Vector2, color: Style): void;
	drawPath(color: Style, path: Vector2[]): void;
	drawInteractionCircle(position: Vector2, selected: boolean): void;
}

class CanvasEngine implements RenderingContext {
	context: CanvasRenderingContext2D;
	camera: Camera;
	mousePosition = new Vector2();
	keys = new Set<number>();

	constructor(public root: IDrawable, public canvas: HTMLCanvasElement) {
		this.root = root;
		this.canvas = canvas;
		this.context = canvas.getContext("2d");
		this.camera = new Camera(this.canvas);

		window.requestAnimationFrame(() => this.draw());
		setInterval(() => this.update(0.02), 50);

		this.canvas.addEventListener("wheel", (e: WheelEvent) => {
			if (!this.camera.lockZoom) {
				this.camera.zoom += e.deltaY * 0.5;
				console.log(`${this.camera.zoom} zoom, ${this.camera.scale} scale`);
			}
		});

		this.canvas.addEventListener("mousemove", (evt) => {
			this.mousePosition = new Vector2(evt.clientX, evt.clientY);
		});

		window.addEventListener("keydown", (evt) => {
			this.keys.add(evt.keyCode);
		});

		window.addEventListener("keyup", (evt) => {
			this.keys.delete(evt.keyCode);
		});
	}

	get isLightMode() {
		return document.body.classList.contains("light_mode");
	}

	resolveStyle(v: Style): HTML5Style {
		if (v && typeof v === "object" && (v as any).dark && (v as any).light) {
			const color = (v as any) as ColorPair;
			return this.isLightMode ? color.light : color.dark;
		}

		return v as HTML5Style;
	}

	getCamera(): Camera {
		return this.camera;
	}

	getMousePosition(): Vector2 {
		return this.mousePosition;
	}

	isKeyDown(code: number): boolean {
		return this.keys.has(code);
	}

	update(dtime: number) {
		this.root.update(this, dtime);
	}

	draw() {
		const parent = this.canvas.parentElement;
		this.canvas.width = parent.clientWidth;
		this.canvas.height = parent.clientHeight;

		this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.root.draw(this);

		window.requestAnimationFrame(() => this.draw());
	}

	drawText(position: Vector2, text: string, fillStyle: Style) {
		const pos = this.camera.worldToScreen(position);

		this.context.textAlign = "center";
		this.context.textBaseline = "middle";
		this.context.fillStyle = this.resolveStyle(fillStyle);
		this.context.fillText(text, pos.x, pos.y);
	}

	drawCircle(position: Vector2, radius: number, fillStyle: Style, strokeStyle: Style = null) {
		const pos = this.camera.worldToScreen(position);

		this.context.fillStyle = this.resolveStyle(fillStyle);
		this.context.lineWidth = 2;
		this.context.strokeStyle = this.resolveStyle(strokeStyle);
		this.context.setLineDash([]);
		this.context.beginPath();
		this.context.arc(pos.x, pos.y, radius * this.camera.scale, 0, Math.PI * 2, true);

		if (fillStyle) {
			this.context.fill();
		}

		if (strokeStyle) {
			this.context.stroke();
		}
	}

	drawCross(position: Vector2, size: number, strokeStyle: Style) {
		const pos = this.camera.worldToScreen(position);

		this.context.lineWidth = 1;
		this.context.strokeStyle = this.resolveStyle(strokeStyle);
		this.context.setLineDash([]);
		this.context.beginPath();

		this.context.moveTo(pos.x - size, pos.y - size);
		this.context.lineTo(pos.x + size, pos.y + size);

		this.context.moveTo(pos.x - size, pos.y + size);
		this.context.lineTo(pos.x + size, pos.y - size);

		this.context.stroke();
	}

	drawLine(from: Vector2, to: Vector2, color: Style, width = 1): void {
		this.context.strokeStyle = this.resolveStyle(color);
		this.context.setLineDash([]);
		this.context.lineWidth = width;

		this.context.beginPath();

		const fromScreen = this.camera.worldToScreen(from);
		this.context.moveTo(fromScreen.x, fromScreen.y);

		const toScreen = this.camera.worldToScreen(to);
		this.context.lineTo(toScreen.x, toScreen.y);

		this.context.stroke();
	}

	drawArrow(from: Vector2, to: Vector2, color: Style): void {
		this.context.strokeStyle = this.resolveStyle(color);
		this.context.setLineDash([]);

		this.context.beginPath();

		const fromScreen = this.camera.worldToScreen(from);
		this.context.moveTo(fromScreen.x, fromScreen.y);

		const toScreen = this.camera.worldToScreen(to);
		this.context.lineTo(toScreen.x, toScreen.y);

		const parallel = toScreen.subtract(fromScreen).normalise().multiply(10);
		const perp = new Vector2(parallel.y, -parallel.x);
		const arrow1 = toScreen.subtract(parallel).add(perp);
		const arrow2 = toScreen.subtract(parallel).subtract(perp);
		this.context.lineTo(arrow1.x, arrow1.y);
		this.context.moveTo(toScreen.x, toScreen.y);
		this.context.lineTo(arrow2.x, arrow2.y);

		this.context.stroke();
	}

	drawPath(color: Style, path: Vector2[]): void {
		this.context.strokeStyle = this.resolveStyle(color);
		this.context.setLineDash([]);

		this.context.beginPath();

		for (let i = 0; i < path.length; i++) {
			const pos = this.camera.worldToScreen(path[i]);
			if (i == 0) {
				this.context.moveTo(pos.x, pos.y);
			} else {
				this.context.lineTo(pos.x, pos.y);
			}
		}

		this.context.stroke();
	}

	drawInteractionCircle(position: Vector2, selected: boolean): void {
		const pos = this.camera.worldToScreen(position);

		if (selected) {
			this.context.lineWidth = 2;
			this.context.strokeStyle = this.resolveStyle(EngineColors.InteractionCircleSelected);
		} else {
			this.context.lineWidth = 1;
			this.context.strokeStyle = this.resolveStyle(EngineColors.InteractionCircle);
		}

		this.context.setLineDash([4,2]);
		this.context.beginPath();
		this.context.arc(pos.x, pos.y, 10, 0, Math.PI * 2, true);
		this.context.stroke();
	}
}

export function createEngineInElement(root: IDrawable, parent: Element): Engine {
	const ce = document.createElement("canvas") as HTMLCanvasElement;
	ce.id = "canvas";
	parent.appendChild(ce);

	return new CanvasEngine(root, ce);
}
