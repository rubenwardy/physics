import { createEngineInElement, Engine, IDrawable, Tool, RenderingContext } from "./Engine";

export class BaseApp implements IDrawable {
	isPaused = false;

	protected engine: Engine;
	private _tool?: Tool = null;

	get tool(): Tool {
		if (this._tool && this._tool.isFinished) {
			this.tool = null;
		}

		return this._tool;
	}

	set tool(v: Tool) {
		if (this._tool) {
			this._tool.onDeactivate();
		}

		this._tool = v;

		if (this._tool) {
			this._tool.onActivate();
		}
	}

	constructor() {
		const body = document.getElementsByTagName("body")[0];
		this.engine = createEngineInElement(this, body);
	}

	update(engine: Engine, dtime: number): void {
		if (this.tool) {
			this.tool.update(engine, dtime);
		}
	}

	draw(context: RenderingContext): void {
		if (this.tool) {
			this.tool.draw(context);
		}
	}
}
