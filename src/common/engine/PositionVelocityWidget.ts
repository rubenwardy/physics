import { Tool, RenderingContext, Engine } from "common/engine";
import { RigidBody } from "common/calc/RigidBody";
import { Vector2 } from "common/calc/Vector2";
import { EngineColors } from "./Engine";

export enum PositionVelocitySelection {
	None,
	Position,
	Velocity
}

export abstract class PositionVelocityWidget implements Tool {
	private _deferredOnDeactivate: (() => void)[] = [];
	protected get deferredOnDeactivate(): (() => void)[] {
		return this._deferredOnDeactivate;
	}

	abstract getStatusText(): string;
	protected abstract get body(): RigidBody;

	protected onHandleChange(selection: PositionVelocitySelection) {}

	protected get baseOffset(): Vector2 {
		return new Vector2();
	}

	protected selection = PositionVelocitySelection.None;
	isFinished = false;


	constructor(protected engine: Engine,
		private readonly velocityScaleMultiplier: number = 10) {}

	onActivate(): void {
		const canvas = document.getElementById("canvas");

		const onMouseDown = () => {
			const mousePosition = this.engine.getMousePosition();
			const body = this.body;

			const offset = this.baseOffset;
			const positionScreen =
					this.engine.getCamera().worldToScreen(body.position.add(offset));
			if (positionScreen.sqdist(mousePosition) < 100) {
				this.selection = PositionVelocitySelection.Position;
				return;
			}

			const velocityPosition = body.position.add(offset)
					.add(body.velocity.multiply(this.velocityScaleMultiplier));
			const velocityScreen = this.engine.getCamera().worldToScreen(velocityPosition);
			if (velocityScreen.sqdist(mousePosition) < 100) {
				this.selection = PositionVelocitySelection.Velocity;
				return;
			}
		};

		const onMouseUp = () => {
			this.selection = PositionVelocitySelection.None;
		};

		canvas.addEventListener("mousedown", onMouseDown);
		canvas.addEventListener("mouseup", onMouseUp);

		this.deferredOnDeactivate.push(() => {
			canvas.removeEventListener("mousedown", onMouseDown);
			canvas.removeEventListener("mouseup", onMouseUp);
		});

		this.selection = PositionVelocitySelection.None;
	}

	onDeactivate(): void {
		this.deferredOnDeactivate.forEach(cb => cb());
		this._deferredOnDeactivate = [];
	}

	update(engine: Engine, dtime: number): void {
	}

	private updateSelection(engine: Engine) {
		if (this.selection == PositionVelocitySelection.None) {
			return;
		}

		const body = this.body;

		let pos = engine.getCamera().screenToWorld(engine.getMousePosition());
		if (!pos) {
			return;
		}

		pos = pos.subtract(this.baseOffset);

		switch (this.selection) {
		case PositionVelocitySelection.Position:
			body.position = pos.round();
			break;
		case PositionVelocitySelection.Velocity:
			body.velocity = pos.subtract(body.position).divide(this.velocityScaleMultiplier);
			break;
		}

		this.onHandleChange(this.selection);
	}

	draw(context: RenderingContext): void {
		const body = this.body;

		this.updateSelection(context);

		const position = body.position.add(this.baseOffset);

		const velocityPosition = position.add(body.velocity.multiply(this.velocityScaleMultiplier));
		context.drawArrow(position, velocityPosition, EngineColors.WidgetArrow);

		context.drawInteractionCircle(position, this.selection == PositionVelocitySelection.Position);
		context.drawInteractionCircle(velocityPosition, this.selection == PositionVelocitySelection.Velocity);
	}
}
