import { Vector2 } from "common/calc/Vector2";

export class Camera {
	center = new Vector2();
	private _zoom = 0;
	private _maxZoom = 100;
	lockZoom = false;
	flipY = false;

	constructor(public canvas: HTMLCanvasElement) {}

	get zoom(): number {
		return this._zoom;
	}

	set zoom(v: number) {
		this._zoom = Math.min(Math.max(v, 0.8), this._maxZoom);
	}

	get scale(): number {
		if (this.zoom > 0) {
			return 10 / (this.zoom * this.zoom);
		} else {
			return 0.003;
		}
	}

	set scale(scale: number) {
		this.zoom = Math.sqrt(10 / scale);
	}

	get maxScale(): number {
		return 10 / (this._maxZoom * this._maxZoom);
	}

	set maxScale(scale: number) {
		this._maxZoom = Math.sqrt(10 / scale);
	}

	worldToScreen(position: Vector2): Vector2 {
		if (this.flipY) {
			position = new Vector2(position.x, -position.y);
		}

		const screenCenter = new Vector2(this.canvas.width / 2, this.canvas.height / 2);
		return position.subtract(this.center)
				.multiply(this.scale)
				.add(screenCenter);
	}

	screenToWorld(position: Vector2): Vector2 {
		const screenSize = new Vector2(this.canvas.width, this.canvas.height);
		if (position.x < 0 || position.y < 0 ||
				position.x > screenSize.x || position.y > screenSize.y) {
			return null;
		}

		const screenCenter = screenSize.divide(2);
		const ret = position.subtract(screenCenter).divide(this.scale).add(this.center);
		if (this.flipY) {
			return new Vector2(ret.x, -ret.y);
		} else {
			return ret;
		}
	}
}
