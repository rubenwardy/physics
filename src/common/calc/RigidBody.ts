import { Vector2 } from "./Vector2";

export interface RigidBody {
	position: Vector2;
	velocity: Vector2;
}
