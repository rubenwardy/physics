import { Forces } from "./Forces";
import { RigidBody } from "./RigidBody";
import { Vector2 } from "./Vector2";

/**
 * A sun, planet, moon, or satellite - something that exerts gravity.
 */
export class Body implements RigidBody {
	/**
	 * Radius, 1000 km.
	 */
	radius: number;

	/**
	 * Mass, in 10^20 kg.
	 */
	mass: number;

	/**
	 * Position, 1000 km.
	 */
	position: Vector2;

	/**
	 * Velocity, in 1000 km per day.
	 */
	velocity: Vector2;

	color: string;
	label: string;

	/**
	 * @param radius in 1000 km
	 * @param mass in 10^20 kg
	 * @param position in 1000 km
	 */
	constructor(radius: number, mass: number,
			position: Vector2 = new Vector2(), velocity: Vector2 = new Vector2(),
			color: string = "white", label: string = null) {
		this.radius = radius;
		this.mass = mass;
		this.position = position;
		this.velocity = velocity;
		this.color = color;
		this.label = label;
	}

	clone(): Body {
		return new Body(this.radius, this.mass,
			this.position, this.velocity,
			this.color, this.label);
	}
}


/**
 * UniversalGravityConstant, in 1000km3 10^20kg-1 s-2.
 *
 *    6.67408 * 10^-11         m3 kg-1      s-2
 *                                               / 10^8
 *  = 6.67408 * 10^-19    1000km3 kg-1      s-2
 *                                               * 10^20
 *  = 66.7408             1000km3 10^20kg-1 s-2
 *                                               * 0.864 ^ 2
 */
const UniversalGravityConstant = 66.7408 * 0.864 * 0.864;


/**
 * Applies the gravity from body1 onto body2
 *
 * @param forces Forces accumulator utility
 * @param body1 First body
 * @param body2 Second body
 */
function applyGravity(forces: Forces, body1: Body, body2: Body): boolean {
	const delta = body2.position.subtract(body1.position);

	var distsq = delta.sqlength();
	if (distsq < 1)
		distsq = 1;

	var force = UniversalGravityConstant * body1.mass * body2.mass / distsq;

	// TODO: remove weird bouncing behaviour
	const radiussq = Math.pow(body1.radius + body2.radius, 2);
	if (distsq < radiussq) {
		console.error("Bodies collided, here be dragons");
		force *= -1;
	}

	forces.add(delta.normalise().multiply(force));

	return distsq < radiussq;
}


/**
 * Class to hold world state, with a fixed time step.
 */
export class GravityWorld {
	bodies: Body[] = [];
	elapsedTime: number = 0;

	clone(): GravityWorld {
		const world = new GravityWorld();
		world.bodies = this.bodies.map(body => body.clone());
		return world;
	}

	addBody(body: Body): Body {
		if (body.label === null) {
			body.label = `Body ${this.bodies.length + 1}`;
		}

		this.bodies.push(body);
		return body;
	}

	/**
	 *
	 * @param radius Radius, in 1000 km
	 * @param mass x10^20 kg
	 */
	addSun(radius: number, mass: number): Body {
		return this.addBody(new Body(radius, mass, new Vector2(), new Vector2(), "orange", "Sun"));
	}

	/** Simulate world
	 *
	 * @param dtime Time interval to simulate, in days
	 */
	update(dtime: number) {
		this.elapsedTime += dtime;

		const stepSize = 0.5;

		while (this.elapsedTime > stepSize) {
			this.step(stepSize);
			this.elapsedTime -= stepSize;
		}
	}

	private step(dtime: number) {
		this.bodies.map(body => {
			const forces = new Forces(body.mass);
			this.bodies.forEach(other_body => {
				if (body != other_body) {
					applyGravity(forces, body, other_body);
				}
			});
			return [body, forces];
		}).forEach(([body, forces]: [Body, Forces]) => {
			body.velocity = body.velocity.add(forces.getAccerlation().multiply(dtime));
			body.position = body.position.add(body.velocity.multiply(dtime));
		});
	}

	getBounds(): [Vector2, Vector2] {
		let min = new Vector2();
		let max = new Vector2();

		this.bodies.forEach(body => {
			const minPos = body.position.subtract(new Vector2(body.radius, body.radius));
			const maxPos = body.position.add(new Vector2(body.radius, body.radius));
			min = min.min(minPos);
			max = max.max(maxPos);
		});

		return [ min, max ];
	}

	getCenterOfMass(): Vector2 {
		let totalMass = this.bodies.map(body => body.mass).reduce((a, b) => a + b);

		return this.bodies.map(body => body.position.multiply(body.mass))
				.reduce((a, b) => a.add(b))
				.divide(totalMass);
	}
}


/**
 * Simulates world into the future, to show orbit predictions.
 */
export class GravityWorldPrediction {
	private world: GravityWorld;
	paths: Map<Body, Vector2[]>;

	constructor(world: GravityWorld) {
		this.world = world;
		this.update();
	}

	update(ghostBody: Body = null) {
		const tempWorld = this.world.clone();
		if (ghostBody) {
			tempWorld.addBody(ghostBody.clone());
		}

		const paths = new Map<Body, Vector2[]>();
		tempWorld.bodies.forEach(body => {
			paths.set(body, [body.position]);
		});

		for (let x = 0; x < 365 / 2; x += 2) {
			tempWorld.update(2);
			tempWorld.bodies.forEach(body => {
				const path = paths.get(body);
				path.push(body.position);
			});
		}

		this.paths = paths;
	}
}


/**
 * Statistics for planets
 *
 * Note: the units in this array are converted into Body units by createSolarSystem().
 *
 * Source: https://nssdc.gsfc.nasa.gov/planetary/factsheet/
 */
export const Planets = [
	{
		name: "Mercury",
		color: "lightgrey",

		mass: 0.330, // x10^24
		radius: 4.879 / 2, // 1000km
		distanceFromSun: 69.82 * 1000, // 1000km
		orbitalVelocity: 38.86, // kms-1
	},

	{
		name: "Venus",
		color: "#8B91A1",

		mass: 4.87, // x10^24
		radius: 12.104 / 2, // 1000km
		distanceFromSun: 108.9 * 1000, // 1000km
		orbitalVelocity: 34.79, // kms-1
	},

	{
		name: "Earth",
		color: "#006994",

		mass: 5.97, // x10^24
		radius: 12.756 / 2, // 1000km
		distanceFromSun: 152.6 * 1000, // 1000km
		orbitalVelocity: 29.29, // kms-1

		moons: [
			{
				name: "Moon",
				color: "lightgrey",
				mass: 0.073,
				radius: 3.475 / 2,
				distanceFromParent: 406, // 1000km
				orbitalVelocity: 1,
			}
		]
	},

	{
		name: "Mars",
		color: "#c1440e",

		mass: 0.642, // x10^24
		radius: 6.792 / 2, // 1000km
		distanceFromSun: 249.2 * 1000, // 1000km
		orbitalVelocity: 21.97, // kms-1
	},

	{
		name: "Jupiter",
		color: "orange",

		mass: 1898, // x10^24
		radius: 142.984 / 2, // 1000km
		distanceFromSun: 816.6 * 1000, // 1000km
		orbitalVelocity: 12.44, // kms-1
	},

	{
		name: "Saturn",
		color: "#ab604a",

		mass: 568, // x10^24
		radius: 120.536 / 2, // 1000km
		distanceFromSun: 1514.5 * 1000, // 1000km
		orbitalVelocity: 9.7, // kms-1
	},

	{
		name: "Uranus",
		color: "#D5FBFC",

		mass: 86.8, // x10^24
		radius: 51.118 / 2, // 1000km
		distanceFromSun: 3003.6 * 1000, // 1000km
		orbitalVelocity: 6.8, // kms-1
	},

	{
		name: "Neptune",
		color: "#73ACAC",

		mass: 102, // x10^24
		radius: 49.528 / 2, // 1000km
		distanceFromSun: 4545.7 * 1000, // 1000km
		orbitalVelocity: 5.4, // kms-1
	},
];


/**
 * Create GravityWorld representing our solar system.
 */
export function createSolarSystem(): GravityWorld {
	const world = new GravityWorld();

	// Sun
	// Radius: 696,340 km
	// Mass: 1.989 × 10^30 kg
	world.addSun(696.34, 1.989e10);

	Planets.forEach(planet => {
		const body = world.addBody(new Body(planet.radius, planet.mass * 10000,
			new Vector2(planet.distanceFromSun, 0),
			new Vector2(0, -planet.orbitalVelocity * 60 * 60 * 24 / 1000),
			planet.color, planet.name));

		(planet.moons || []).forEach(moon => {
			world.addBody(new Body(moon.radius, moon.mass * 10000,
				body.position.add(new Vector2(moon.distanceFromParent, 0)),
				body.velocity.add(new Vector2(0, -moon.orbitalVelocity * 60 * 60 * 24 / 1000)),
				moon.color, moon.name));
		});
	});

	return world;
}
