import { Vector2 } from "./Vector2";

/**
 * Utility function to make it (slightly) easier to sum forces
 */
export class Forces {
	private sum = new Vector2();

	constructor(readonly mass: number) {}

	reset() {
		this.sum = new Vector2();
	}

	add(force: Vector2) {
		this.sum = this.sum.add(force);
	}

	getAccerlation(): Vector2 {
		return this.sum.divide(this.mass);
	}
}
