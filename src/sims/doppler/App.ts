import { Engine, RenderingContext, BaseApp, ColorPair } from "common/engine";
import { Vector2 } from "common/calc/Vector2";
import { DopplerBottomPanel } from "./BottomPanel";
import World from "./World";
import { EmitterTool } from "./EmitterTool";


export const Colors = {
	Body: { dark: "white", light: "black" },
	Axis: { dark: "#aaa", light: "black" },
};

class App extends BaseApp {
	private bottomPanel: DopplerBottomPanel;
	private world: World;

	constructor() {
		super();

		const body = document.getElementsByTagName("body")[0];
		this.world = new World();
		this.bottomPanel = new DopplerBottomPanel(body);

		this.bottomPanel.onPausePlay(() => {
			this.isPaused = !this.isPaused;
			this.bottomPanel.setPaused(this.isPaused);
		});

		window.addEventListener("keydown", event => {
			if (event.keyCode == 32) {
				this.isPaused = !this.isPaused;
				this.bottomPanel.setPaused(this.isPaused);
			}
		});

		const camera = this.engine.getCamera();
		camera.lockZoom = true;
		camera.maxScale = 1;
		camera.scale = 1;
		camera.flipY = true;

		this.tool = new EmitterTool(this.world, this.engine);
	}

	update(engine: Engine, dtime: number) {
		if (!this.isPaused) {
			this.world.update(dtime);
		}
		super.update(engine, dtime);
	}

	draw(context: RenderingContext) {
		const camera = context.getCamera();
		camera.center = new Vector2(window.innerWidth, 0)
				.divide(2 * camera.scale).add(new Vector2(-150, 0));

		const left = camera.screenToWorld(new Vector2(0.1, 10)).x;
		context.drawLine(new Vector2(left, 0),
			new Vector2(left + 2000 / camera.scale, 0), Colors.Axis, 2);

		this.world.emitters.forEach(emitter => {
			context.drawCircle(emitter.position, emitter.initialRadius, Colors.Body);
		});

		this.world.circles.forEach(circle => {
			const color: ColorPair = {
				dark: `rgba(255, 255, 255, ${circle.volume * 0.5})`,
				light: `rgba(0, 0, 0, ${circle.volume * 0.5})`,
			}

			context.drawCircle(circle.position, circle.radius,
				"transparent", color);
		});

		super.draw(context);

		if (this.tool) {
			this.bottomPanel.setStatus(this.tool.getStatusText());
		} else {
			this.bottomPanel.setStatus(`Ready`);
		}
	}
}

window.addEventListener("load", () => new App());
