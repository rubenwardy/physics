import { Vector2 } from "common/calc/Vector2";
import { Engine, RenderingContext, Panel, createElement, PositionVelocitySelection, PositionVelocityWidget } from "common/engine";
import World, { Emitter } from "./World";

export class EmitterTool extends PositionVelocityWidget {
	protected body: Emitter;
	private panel: Panel<Emitter>;
	private panelRoot: HTMLElement;

	constructor(public world: World, engine: Engine) {
		super(engine, 1);

		this.body = new Emitter();

		const onKeyDown = (event: KeyboardEvent) => {
			if (event.keyCode == 13) {
				this.spawn();
			}
		}

		window.addEventListener("keyup", onKeyDown);

		this.deferredOnDeactivate.push(() => {
			window.removeEventListener("keyup", onKeyDown);
		});
	}

	getStatusText(): string {
		return "Press ENTER to spawn an emitter";
	}

	protected onHandleChange(selection: PositionVelocitySelection) {
		this.body.position =
			new Vector2(Math.max(this.body.position.x, 0), this.body.position.y);
	}

	onActivate(): void {
		super.onActivate();

		this.panelRoot = createElement(`<div class="dialog-transparent"></div>`);
		document.getElementById("canvas").parentElement.appendChild(this.panelRoot);

		this.panel = new Panel<Emitter>(this.panelRoot);
		const build = this.panel.edit();

		build.addVector("velocity", "Velocity", "ms<sup>-1</sup>");

		this.body = this.panel.bind(this.body);
	}

	onDeactivate() {
		super.onDeactivate();

		this.panel.remove();
		this.body = new Emitter();
	}

	spawn() {
		this.world.clear();
		this.world.emitters.push(this.body.clone());
	}

	draw(context: RenderingContext): void {
		if (this.world.emitters.length == 0) {
			context.drawCircle(this.body.position, 0.5, "white");
		}

		const bodyPos = context.getCamera().worldToScreen(this.body.position);
		const linePos = context.getCamera().worldToScreen(new Vector2(0, -50));
		this.panelRoot.style.left = `${bodyPos.x - this.panelRoot.clientWidth / 2}px`;
		this.panelRoot.style.top = `${linePos.y}px`;

		if (this.body.position.y != 0) {
			context.drawLine(new Vector2(0, this.body.position.y),
				new Vector2(-1.6, this.body.position.y), "white");
			context.drawText(new Vector2(-40, this.body.position.y),
					`${this.body.position.y}`, "white");
		}

		super.draw(context);
	}
}
