import { RigidBody } from "common/calc/RigidBody";
import { Vector2 } from "common/calc/Vector2";

export class Circle {
	position: Vector2;
	radius: number;
	volume: number;
}

export class Emitter implements RigidBody {
	position = new Vector2(0, 0);
	velocity = new Vector2(250, 0);

	initialRadius = 10;
	interval = 0.2;
	timeSinceLast = 0.21

	clone() {
		const emitter = new Emitter();
		emitter.position = this.position;
		emitter.velocity = this.velocity;
		emitter.initialRadius = this.initialRadius;
		emitter.interval = this.interval;
		emitter.timeSinceLast = this.timeSinceLast;
		return emitter;
	}

	get speed(): number {
		return this.velocity.length();
	}

	get angle(): number {
		return Math.atan2(this.velocity.y, this.velocity.x) * 180 / Math.PI;
	}

	set speed(v: number) {
		this.velocity = this.velocity.normalise().multiply(v);
	}

	set angle(v: number) {
		v = v * Math.PI / 180;

		const speed = this.speed;
		this.velocity = new Vector2(speed * Math.cos(v), speed * Math.sin(v));
	}
}

export default class World {
	circles: Circle[] = [];
	emitters: Emitter[] = [];

	constructor() {
		this.emitters.push(new Emitter());
	}

	update(dtime: number) {
		this.circles.forEach(circle => {
			circle.radius += 300 * dtime;
			circle.volume -= 0.25 * dtime;
		});

		this.emitters.forEach(emitter => {
			emitter.timeSinceLast += dtime;
			while (emitter.timeSinceLast > emitter.interval) {
				emitter.timeSinceLast -= emitter.interval;
				this.addCircle(emitter.position, emitter.initialRadius);
			}

			emitter.position =
					emitter.position.add(emitter.velocity.multiply(dtime));
		});

		this.clean();
	}

	clean() {
		this.circles = this.circles.filter(circle => circle.volume > 0.05);
		this.emitters = this.emitters.filter(emitter => emitter.position.sqlength() <= 6000000);
	}

	clear() {
		this.circles = [];
		this.emitters = [];
	}

	addCircle(position: Vector2, radius: number) {
		this.circles.push({
			position: position,
			radius: radius,
			volume: 1,
		});
	}
}
