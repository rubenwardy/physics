import { WorldView } from "./WorldView";
import { RenderingContext, Engine, Dialog, PositionVelocityWidget, PositionVelocitySelection } from "common/engine";
import { Body } from "common/calc/Gravity";
import { Vector2 } from "common/calc/Vector2";

export class SpawnBodyTool extends PositionVelocityWidget {
	protected body: Body;
	private dialog: Dialog<Body>;

	protected get baseOffset(): Vector2 {
		return this.worldView.cameraTarget.position;
	}

	constructor(protected worldView: WorldView, engine: Engine) {
		super(engine);
	}

	onActivate(): void {
		super.onActivate();

		if (this.worldView.cameraTarget.label == "Sun") {
			this.body =
					new Body(6, 5970, new Vector2(0, 150880), new Vector2(-0.010*60*60*24, -0.080*60*60*24), "lightblue");
		} else {
			this.body =
					new Body(0.1, 1, new Vector2(0, 6), new Vector2(0, 0.010*60*60*24), "lightblue");
		}

		this.worldView.ghostBodyProvider = () => this.createGhost();

		const onKeyDown = (event: KeyboardEvent) => {
			if (event.keyCode == 13) {
				this.spawn();
			}
		}

		window.addEventListener("keyup", onKeyDown);

		this.deferredOnDeactivate.push(() => {
			window.removeEventListener("keyup", onKeyDown);
		});

		this.dialog = new Dialog<Body>("New Body");

		const build = this.dialog.edit();

		this.dialog.onClose(() => {
			this.isFinished = true;
		});

		{
			const row = build.row();
			row.addTextInput("label", "Label");
			row.addTextInput("color", "Colour");
		}

		build.addVector("position", "Position", "1000 km");
		build.addVector("velocity", "Velocity", "1000 km/day");

		const onMassRadiusChanged = () => {
			this.updateDialog();
		}

		{
			const row = build.row();
			row.addNumberInput("mass", "Mass", "10<sup>20</sup> kg").onChange(onMassRadiusChanged);
			row.addNumberInput("radius", "Radius", "1000 km").onChange(onMassRadiusChanged);
		}

		build.append("<p id='dialog-relative-size'></p>");
		this.updateDialog();

		build.addButton("Spawn", () => {
			this.spawn();
		});

		this.body = this.dialog.bind(this.body);
	}

	onDeactivate(): void {
		super.onDeactivate();

		this.worldView.ghostBodyProvider = null;
		this.dialog.remove();
	}

	getStatusText(): string {
		switch (this.selection) {
		case PositionVelocitySelection.None:
			return "Enter to spawn, Mouse to move, Esc to cancel";
		case PositionVelocitySelection.Position:
			return "Moving, release to drop. Esc to cancel";
		case PositionVelocitySelection.Velocity:
			return "Changing velocity, release mouse to stop. Esc to cancel";
		}
	}

	update(engine: Engine, dtime: number): void {
		super.update(engine, dtime);
	}

	private updateDialog() {
		const relative = document.getElementById("dialog-relative-size");
		const relMass = Math.round(10000 * this.body.mass / 5970) / 100;
		const relRadius = Math.round(10000 * this.body.radius / 6) / 100;
		relative.innerText =
			`= ${relMass}% mass and ${relRadius}% radius of Earth.`;

		const number = this.worldView.world.bodies.length + 1;
		this.dialog.setPlaceholder("label", `Body ${number}`);
	}

	private spawn() {
		this.worldView.world.addBody(this.createGhost());
		this.updateDialog();
	}

	private createGhost(): Body {
		const target = this.worldView.cameraTarget;

		const ghost = this.body.clone();
		ghost.position = ghost.position.add(target.position);
		ghost.velocity = ghost.velocity.add(target.velocity);
		return ghost;
	}

	draw(context: RenderingContext): void {
		super.draw(context);

		const body = this.body;
		const target = this.worldView.cameraTarget;
		const offset = target.position;

		context.drawArrow(offset, offset.add(target.velocity.multiply(10)), "rgba(255,255,255,0.4)");

		const absVelocityPosition = body.position.add(offset).add(body.velocity.add(target.velocity).multiply(10));
		context.drawArrow(body.position.add(offset), absVelocityPosition, "rgba(255,255,255,0.6)");
	}
}
