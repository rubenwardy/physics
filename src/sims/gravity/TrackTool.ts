import { WorldView } from "./WorldView";
import { Body } from "common/calc/Gravity";
import { Tool, RenderingContext, Engine, Dialog } from "common/engine";
import { Vector2 } from "common/calc/Vector2";

export class TrackTool implements Tool {
	private removeEventListeners: () => void;
	isFinished = false;

	dialog: Dialog<any>;

	constructor(private worldView: WorldView, private engine: Engine) {}

	onActivate(): void {
		const canvas = document.getElementById("canvas");

		const onMouseUp = () => {
			const body = this.getSelectedBody();
			if (!body) {
				return;
			}

			this.track(body);
			this.isFinished = true;
		};

		canvas.addEventListener("mouseup", onMouseUp);
		this.removeEventListeners = () => {
			canvas.removeEventListener("mouseup", onMouseUp);
		}

		this.dialog = new Dialog<any>("Track");

		const build = this.dialog.edit()
		build.append(`
			<p>
				Select a body to follow.
			</p>
		`);

		const row = build.multiRow();
		this.worldView.world.bodies.forEach((body, i) => {
			row.addButton(`
				<span class="body-icon" style="background-color: ${body.color};"></span>
				${body.label}
			`, () => this.track(body));
		})
	}

	onDeactivate(): void {
		this.removeEventListeners();
		this.removeEventListeners = () => {};
		this.dialog.remove();
	}

	getStatusText(): string {
		return `Tracking ${this.worldView.cameraTarget.label}. Click a body to track it, Esc to cancel.`;
	}

	update(engine: Engine, dtime: number): void {
	}

	private track(body: Body) {
		this.worldView.cameraTarget = body;
		this.worldView.cameraOffset = new Vector2();
	}

	private getSelectedBody(): Body {
		return this.worldView.getHoveredBody(this.engine.getCamera(),
				this.engine.getMousePosition());
	}

	draw(context: RenderingContext): void {
		const selectedBody = this.getSelectedBody();
		this.worldView.world.bodies.forEach(body => {
			context.drawInteractionCircle(body.position, selectedBody == body);
		});
	}
}
