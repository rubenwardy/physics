import { WorldView } from "./WorldView";
import { Engine, appendElement } from "common/engine";
import { Body } from "common/calc/Gravity";

class StatsSet {
	current = 0;
	min = 100000000;
	max = -100000000;

	update(current: number) {
		this.current = current;
		this.min = Math.min(this.min, current);
		this.max = Math.max(this.max, current);
	}

	scale(factor: number): StatsSet {
		const ret = new StatsSet();
		ret.current = this.current * factor;
		ret.min = this.min * factor;
		ret.max = this.max * factor;
		return ret;
	}
}

class BodyStats {
	readonly speed = new StatsSet();
	readonly distanceFromParent = new StatsSet();

	constructor(private body: Body) {}

	update() {
		this.speed.update(this.body.velocity.length());
		this.distanceFromParent.update(this.body.position.length());
	}
}

export class BodyToolTip {
	panel: HTMLElement;
	stats = new Map<Body, BodyStats>();

	constructor(private worldView: WorldView, root: HTMLElement) {
		this.panel = appendElement(root, `
			<aside class="panel panel-info">
			</aside>
		`);
	}

	update(engine: Engine, dtime: number) {
		this.worldView.world.bodies.forEach(body => {
			let stats = this.stats.get(body);
			if (!stats) {
				stats = new BodyStats(body);
				this.stats.set(body, stats);
			}

			stats.update();
		});

		const makeStats = (body: Body, trackedBody: Body) => {
			const stats = this.stats.get(body);
			const speed = stats.speed.scale(1000 / 60 / 60 / 24);
			const distance = stats.distanceFromParent;

			let trackedHTML = "";
			if (trackedBody != body) {
				const delta = trackedBody.position.subtract(body.position);

				trackedHTML = `
					<tr>
						<td>
							<span class="body-icon" style="background-color: ${trackedBody.color};"></span>
							To ${trackedBody.label}:
						</th>
						<td>
							<b>${delta.length().toFixed(0)}</b> 1000km</sup>
							at <b>${delta.angleDeg().toFixed(1)}</b> &deg;
						</td>
					</tr>
				`;
			}


			return `
				<table class="table">
					<tr>
						<th colspan=2 class="left">
							<span class="body-icon" style="background-color: ${body.color};"></span>
							${body.label}
						</th>
					</tr>
					<tr>
						<td>Position:</th>
						<td><b>${body.position.round()}</b></td>
					</tr>
					<tr>
						<td>Velocity:</th>
						<td><b>${speed.current.toFixed(1)}</b> kms<sup>-1</sup>
							at <b>${body.velocity.angleDeg().toFixed(1)}</b>&deg; <br>
							(min ${speed.min.toFixed(1)}, max ${speed.max.toFixed(1)})</td>
					</tr>
					<tr>
						<td>Orbit<br>Distance:</th>
						<td><b>${distance.current.toFixed(0)}</b> 1000km</sup><br>
							(min ${distance.min.toFixed(0)}, max ${distance.max.toFixed(0)})</td>
					</tr>
					${trackedHTML}
				</table>
			`;
		}

		const makeShortStats = (body: Body) => {
			const stats = this.stats.get(body);
			const speed = stats.speed.scale(1000 / 60 / 60 / 24);
			const distance = stats.distanceFromParent.scale(1 / 1000);
			return `
				<tr>
					<td>
						<span class="body-icon" style="background-color: ${body.color};"></span>
						${body.label}
					</td>
					<td class="center">${speed.current.toFixed(1)}</td>
					<td class="center">${speed.min.toFixed(1)}</td>
					<td class="center">${speed.max.toFixed(1)}</td>
					<td class="center">${distance.current.toFixed(0)}</td>
					<td class="center">${distance.min.toFixed(0)}</td>
					<td class="center">${distance.max.toFixed(0)}</td>
				</td>
			`;
		}

		let body = this.worldView.getHoveredBody(engine.getCamera(),
				engine.getMousePosition(), 40);
		if (!body) {
			body = this.worldView.cameraTarget;
		}

		if (body.label == "Sun") {
			const data = this.worldView.world.bodies.map(makeShortStats).join("\n");
			this.panel.innerHTML = `
				<table class='table'>
					<tr>
						<th class="left">Body</th>
						<th colspan=3>
							Orbital Speed
							<span class="unit">kms<sup>-1</sup></span>
						</th>
						<th colspan=3>
							Orbital Distance
							<span class="unit">Gm</span>
						</th>
					</tr>
					<tr>
						<th></th>
						<th>Cur</th>
						<th>Min</th>
						<th>Max</th>
						<th>Cur</th>
						<th>Min</th>
						<th>Max</th>
					</tr>
					${data}
				</table>
			`;
			this.panel.classList.remove("panel-info-min-width");
		} else {
			this.panel.innerHTML = makeStats(body, this.worldView.cameraTarget);
			this.panel.classList.add("panel-info-min-width");
		}
	}
}
