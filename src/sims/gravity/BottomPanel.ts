import { BottomPanel } from "common/engine";

export class GravityBottomPanel extends BottomPanel {
	constructor(public parent: Element) {
		super(parent);

		this.addPausePlay();
		this.addSpeedChange();
		this.addToggleTheme();

		this.addSeparator();

		this.addButton("New Body");
		this.addButton("Track");

		this.addSeparator();

		this.addStatus();
	}

	onNewBody(callback: () => void) {
		this.on("New Body", callback);
	}

	onTrackTool(callback: () => void) {
		this.on("Track", callback);
	}
}
