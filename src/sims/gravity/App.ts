import { Engine, RenderingContext, BaseApp } from "common/engine";
import { WorldView } from "./WorldView";
import { GravityBottomPanel } from "./BottomPanel";
import { SpawnBodyTool } from "./SpawnBodyTool";
import { TrackTool } from "./TrackTool";
import { BodyToolTip } from "./BodyToolTip";

export const Colors = {
	BodyPrediction: { dark: "#666", light: "#aaa" },
};

class App extends BaseApp {
	private _timeSpeed = 30;
	private worldView: WorldView;
	private bottomPanel: GravityBottomPanel;
	private bodyToolTip: BodyToolTip;

	constructor() {
		super();

		const body = document.getElementsByTagName("body")[0];
		this.worldView = new WorldView();
		this.bottomPanel = new GravityBottomPanel(body);
		this.bodyToolTip = new BodyToolTip(this.worldView, body);

		// Init scale
		const [min, max] = this.worldView.world.getBounds();
		const size = max.subtract(min);
		const length = Math.max(size.x, size.y);
		this.engine.getCamera().scale = 0.002;
		// this.engine.camera.scale = 800 / length;
		this.engine.getCamera().maxScale = 400 / length;

		// Bottom panel callbacks
		this.bottomPanel.onPausePlay(() => {
			this.isPaused = !this.isPaused;
			this.bottomPanel.setPaused(this.isPaused);
		});

		this.bottomPanel.onSpeedChange((increased) => {
			let step = 1;
			if (this._timeSpeed >= 200) {
				step = 50;
			} else if (this._timeSpeed >= 50) {
				step = 20;
			} else if (this._timeSpeed >= 30) {
				step = 10;
			} else if (this._timeSpeed >= 10) {
				step = 5;
			}

			this._timeSpeed += (increased ? 1 : -1) * step;
			this._timeSpeed = Math.max(1, this._timeSpeed);
			this.bottomPanel.setTimeSpeed(this._timeSpeed);
		});

		window.addEventListener("keydown", event => {
			if (event.keyCode == 32) {
				this.isPaused = !this.isPaused;
				this.bottomPanel.setPaused(this.isPaused);
			} else if (event.keyCode == 27) {
				this.tool = null;
			}
		});

		this.bottomPanel.onNewBody(() => {
			this.tool = new SpawnBodyTool(this.worldView, this.engine);
		})

		this.bottomPanel.onTrackTool(() => {
			this.tool = new TrackTool(this.worldView, this.engine);
			this.isPaused = true;
			this.bottomPanel.setPaused(true);
		})
	}

	/**
	 * In-world days per real-life seconds
	 */
	get timeSpeed(): number {
		return this.isPaused ? 0 : this._timeSpeed;
	}

	update(engine: Engine, dtime: number): void {
		if (this.timeSpeed > 0) {
			this.worldView.world.update(dtime * this.timeSpeed);
		}

		this.worldView.update(engine, dtime);
		super.update(engine, dtime);
		this.bodyToolTip.update(engine, dtime);
	}

	draw(context: RenderingContext): void {
		this.worldView.draw(context);

		super.draw(context);

		if (this.tool) {
			this.bottomPanel.setStatus(this.tool.getStatusText());
		} else {
			this.bottomPanel.setStatus(`Tracking ${this.worldView.cameraTarget.label}. WASD/arrows to pan, Scroll to zoom`);
		}
	}
}

window.addEventListener("load", () => new App());
