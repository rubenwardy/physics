import { IDrawable, Engine, RenderingContext, Camera } from "common/engine";
import { GravityWorld, GravityWorldPrediction, Body, createSolarSystem } from "common/calc/Gravity";
import { Vector2 } from "common/calc/Vector2";
import { Colors } from "./App";

function scaleRadius(radius: number, cameraScale: number): number {
	const inverseScale = 1 / (cameraScale * 0.1);
	return inverseScale * Math.max(1 - Math.pow(2.72, -radius / 10), 0.1);
}

export class WorldView implements IDrawable {
	world: GravityWorld;
	cameraTarget: Body;
	cameraOffset: Vector2 = new Vector2();
	ghostBodyProvider?: () => Body = null;
	prediction: GravityWorldPrediction;

	get ghostBody(): Body {
		return this.ghostBodyProvider ? this.ghostBodyProvider() : null;
	}

	constructor() {
		this.world = createSolarSystem();
		this.cameraTarget = this.world.bodies[0];
		this.prediction = new GravityWorldPrediction(this.world);
	}

	update(engine: Engine, dtime: number): void {
		this.prediction.update(this.ghostBody ? this.ghostBody : null);
		this.moveCamera(engine, dtime);
	}

	draw(context: RenderingContext): void {
		context.getCamera().center = this.cameraTarget.position.add(this.cameraOffset);

		this.prediction.paths.forEach((values, body) => {
			context.drawPath(Colors.BodyPrediction, values);
		});

		context.drawCross(this.world.getCenterOfMass(), 10, "white");

		const drawBody = (body: Body) => {
			context.drawCircle(body.position, scaleRadius(body.radius, context.getCamera().scale), null, body.color);
			context.drawCircle(body.position, body.radius, body.color);
		};

		this.world.bodies.forEach(drawBody);

		if (this.ghostBody) {
			drawBody(this.ghostBody);
		}
	}

	getHoveredBody(camera: Camera, mousePosition: Vector2, targetSize: number = 20): Body {
		const pos = camera.screenToWorld(mousePosition);
		if (!pos) {
			return null;
		}

		const limit = targetSize / camera.scale;
		const sqlimit = limit * limit;

		return this.world.bodies.find((body) => {
			return body.position.sqdist(pos) < sqlimit;
		});
	}

	private moveCamera(engine: Engine, dtime: number) {
		let direction = new Vector2();

		// Up
		if (engine.isKeyDown(38) || engine.isKeyDown(87)) {
			direction = new Vector2(0, -1);

		// Right
		} else if (engine.isKeyDown(39) || engine.isKeyDown(68)) {
			direction = new Vector2(1, 0);

		// Down
		} else if (engine.isKeyDown(40) || engine.isKeyDown(83)) {
			direction = new Vector2(0, 1);

		// Left
		} else if (engine.isKeyDown(37) || engine.isKeyDown(65)) {
			direction = new Vector2(-1, 0);
		} else {
			return;
		}

		this.cameraOffset = this.cameraOffset.add(direction.multiply(1000 / engine.getCamera().scale * dtime));
	}
}
