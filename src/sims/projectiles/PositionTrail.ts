import { Vector2 } from "common/calc/Vector2";
import { IDrawable, Engine, RenderingContext } from "common/engine";
import { Colors } from "./App";
import { Projectile, RigidWorld } from "./World";

export class PositionTrail implements IDrawable {
	private points: Vector2[] = [];
	private timeSinceLastAddition = 0;
	private lastBody?: Projectile = null;

	constructor(private world: RigidWorld) {}

	update(engine: Engine, dtime: number) {
		if (this.lastBody != this.world.bodies[0]) {
			this.lastBody = this.world.bodies[0];
			if (this.world.bodies.length > 0) {
				this.points = [];
				this.timeSinceLastAddition = 10;
			}
		}

		this.timeSinceLastAddition += dtime;
		if (this.timeSinceLastAddition >= 0.05) {
			this.world.bodies.filter(body => body.position.y >= 0).forEach(body => this.points.push(body.position));
			this.timeSinceLastAddition = 0;
		}
	}

	draw(context: RenderingContext) {
		const color = Colors.Trail;

		const radius = 0.2;

		let previous: Vector2 = null;
		this.points.forEach(point => {
			if (previous && point.y < previous.y) {
				context.drawCircle(new Vector2(-3.5, point.y), radius, color);
			} else {
				context.drawCircle(new Vector2(-4.5, point.y), radius, color);
			}

			previous = point;
		});

		const top = context.getCamera().screenToWorld(new Vector2(0, 50)).y;
		this.points.forEach(point =>
				context.drawCircle(new Vector2(point.x, top), radius, color));

		context.drawPath(color, this.points);

		const mp = context.getMousePosition();
		const mpW = context.getCamera().screenToWorld(mp);
		if (mpW) {
			const point = this.findClosestPoint(mpW.x);
			if (point) {
				context.drawLine(new Vector2(point.x, 0), point, Colors.TrailLine);
				context.drawText(new Vector2(point.x + 0.75, 0.5), `${point.y.toFixed(1)}`, Colors.TrailLine);
			}
		}
	}

	findClosestPoint(x: number): Vector2 {
		let closest: Vector2 = null;
		let closestDist: number = 1000000;

		this.points.forEach(point => {
			const dist = Math.abs(point.x - x);
			if (dist < closestDist) {
				closest = point;
				closestDist = dist;
			}
		})

		return closest;
	}
}
