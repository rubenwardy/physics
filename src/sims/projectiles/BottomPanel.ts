import { BottomPanel } from "common/engine";

export class ProjectilesBottomPanel extends BottomPanel {
	constructor(public parent: Element) {
		super(parent);

		this.addPausePlay();
		this.addToggleTheme();
		this.addSeparator();
		this.addStatus();
	}

	setStatus(status: string) {
		const ele = document.getElementById("status");
		ele.innerText = status;
	}
}
