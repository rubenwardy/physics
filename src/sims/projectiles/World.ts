import { Forces } from "common/calc/Forces";
import { RigidBody } from "common/calc/RigidBody";
import { Vector2 } from "common/calc/Vector2";

export class Projectile implements RigidBody {
	constructor(public mass: number, public position: Vector2, public velocity: Vector2) {}

	get speed(): number {
		return this.velocity.length();
	}

	get angle(): number {
		return Math.atan2(this.velocity.y, this.velocity.x) * 180 / Math.PI;
	}

	set speed(v: number) {
		this.velocity = this.velocity.normalise().multiply(v);
	}

	set angle(v: number) {
		v = v * Math.PI / 180;

		const speed = this.speed;
		this.velocity = new Vector2(speed * Math.cos(v), speed * Math.sin(v));
	}


	update(dtime: number) {
		const forces = new Forces(this.mass);
		forces.add(new Vector2(0, -9.81 * this.mass));
		this.velocity = this.velocity.add(forces.getAccerlation().multiply(dtime));
		this.position = this.position.add(this.velocity.multiply(dtime));
	}

	clone(): Projectile {
		return new Projectile(this.mass, this.position, this.velocity);
	}
}

export class RigidWorld {
	bodies: Projectile[] = [];

	update(dtime: number) {
		this.bodies.forEach(body =>  body.update(dtime));
	}

	removeOutOfRange() {
		this.bodies = this.bodies.filter(body => body.position.y > -30);
	}
}
