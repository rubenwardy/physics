import { Engine, RenderingContext, BaseApp } from "common/engine";
import { ProjectilesBottomPanel } from "./BottomPanel";
import { RigidWorld } from "./World";
import { ProjectileTool } from "./ProjectileTool";
import { Vector2 } from "common/calc/Vector2";
import { PositionTrail } from "./PositionTrail";

export const Colors = {
	Ball: { dark: "white", light: "black" },
	Axis: { dark: "#aaa", light: "black" },
	AxisTick: { dark: "#999", light: "#ccc" },
	Trail: { dark: "rgba(255, 255, 255, 0.25)", light: "rgba(0, 0, 0, 0.35)" },
	TrailLine: { dark: "white", light: "black" },
};

class App extends BaseApp {
	private bottomPanel: ProjectilesBottomPanel;
	private world = new RigidWorld();
	private trail = new PositionTrail(this.world);

	constructor() {
		super();

		const body = document.getElementsByTagName("body")[0];
		this.bottomPanel = new ProjectilesBottomPanel(body);
		this.bottomPanel.onPausePlay(() => {
			this.isPaused = !this.isPaused;
			this.bottomPanel.setPaused(this.isPaused);
		});

		window.addEventListener("keydown", event => {
			if (event.keyCode == 32) {
				this.isPaused = !this.isPaused;
				this.bottomPanel.setPaused(this.isPaused);
			}
		});

		const camera = this.engine.getCamera();
		camera.lockZoom = true;
		camera.maxScale = 40;
		camera.scale = 40;
		camera.flipY = true;

		this.tool = new ProjectileTool(this.world, this.engine);
	}

	update(engine: Engine, dtime: number) {
		if (!this.isPaused) {
			this.trail.update(engine, dtime);

			this.world.update(dtime);
			this.world.removeOutOfRange();
		}

		super.update(engine, dtime);
	}

	draw(context: RenderingContext) {
		const camera = context.getCamera();
		camera.center = new Vector2(window.innerWidth, -window.innerHeight + 100)
				.divide(2 * camera.scale).add(new Vector2(-6, 4.5));

		const left = camera.screenToWorld(new Vector2(0.1, 10)).x;
		context.drawLine(new Vector2(left, 0), new Vector2(left + 2000 / camera.scale, 0), Colors.Axis, 2);

		for (let x = 0; x < 100; x++) {
			context.drawLine(new Vector2(x, 0), new Vector2(x, -0.35), Colors.AxisTick);
			if (x % 5 == 0) {
				context.drawText(new Vector2(x, -0.75), `${x}`, Colors.AxisTick);
			}
		}

		this.trail.draw(context);

		this.world.bodies.forEach(body =>
			context.drawCircle(body.position, 0.5, Colors.Ball));

		super.draw(context);
		if (this.tool) {
			this.bottomPanel.setStatus(this.tool.getStatusText());
		} else {
			this.bottomPanel.setStatus(`Ready`);
		}
	}
}

window.addEventListener("load", () => new App());
