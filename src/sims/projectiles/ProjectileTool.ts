import { Vector2 } from "common/calc/Vector2";
import { Engine, RenderingContext, Panel, createElement, PositionVelocitySelection, PositionVelocityWidget, EngineColors } from "common/engine";
import { Colors } from "./App";
import { Projectile, RigidWorld } from "./World";

export class ProjectileTool extends PositionVelocityWidget {
	protected body: Projectile;
	private panel: Panel<Projectile>;
	private panelRoot: HTMLElement;

	constructor(public world: RigidWorld, engine: Engine) {
		super(engine, 0.5);

		this.body = new Projectile(10, new Vector2(0, 0), new Vector2(5, 12));

		const onKeyDown = (event: KeyboardEvent) => {
			if (event.keyCode == 13) {
				this.spawn();
			}
		}

		window.addEventListener("keyup", onKeyDown);

		this.deferredOnDeactivate.push(() => {
			window.removeEventListener("keyup", onKeyDown);
		});
	}

	getStatusText(): string {
		return "Press ENTER to spawn a projectile";
	}

	protected onHandleChange(selection: PositionVelocitySelection) {
		this.body.position = new Vector2(0, Math.max(this.body.position.y, 0));
	}

	onActivate(): void {
		super.onActivate();

		this.panelRoot = createElement(`<div class="dialog-transparent"></div>`);
		document.getElementById("canvas").parentElement.appendChild(this.panelRoot);

		this.panel = new Panel<Projectile>(this.panelRoot);
		const build = this.panel.edit();

		{
			const row = build.row();
			row.addNumberInput("speed", "Speed", "ms<sup>-1</sup>");
			row.addNumberInput("angle", "Angle", "&deg;");
		}

		build.addVector("velocity", "Velocity", "ms<sup>-1</sup>");

		this.body = this.panel.bind(this.body);
	}

	onDeactivate() {
		super.onDeactivate();

		this.panel.remove();
	}

	spawn() {
		this.world.bodies = [];
		this.world.bodies.push(this.body.clone());
	}

	draw(context: RenderingContext): void {
		if (this.world.bodies.length == 0) {
			context.drawCircle(this.body.position, 0.5, Colors.Ball);
		}

		const bodyPos = context.getCamera().worldToScreen(this.body.position);
		const linePos = context.getCamera().worldToScreen(new Vector2(0, -1));
		this.panelRoot.style.left = `${bodyPos.x - this.panelRoot.clientWidth / 2}px`;
		this.panelRoot.style.top = `${linePos.y}px`;

		if (this.body.position.y > 0) {
			context.drawLine(new Vector2(0, this.body.position.y),
				new Vector2(-1.6, this.body.position.y), EngineColors.Text);
			context.drawText(new Vector2(-2, this.body.position.y),
					`${this.body.position.y}`, EngineColors.Text);
		}

		super.draw(context);
	}
}
